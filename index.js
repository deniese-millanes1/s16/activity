console.log("Hello World");

let number = Number(prompt("Type a number"));
console.log("The number you provided is: " + number);

for (let x = number; 0<=x; x--) {
	
	if (x % 10 === 0 && x>50) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	if (x % 5 === 0 && x>50) {
		console.log(x);	
	} 
	if (x % 50 === 0) {
		console.log("The current value is at 50. Terminating the loop");
		break;
	}
}